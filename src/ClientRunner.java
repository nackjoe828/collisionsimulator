import java.io.FileNotFoundException;
import java.net.SocketException;
import java.net.UnknownHostException;


public class ClientRunner {
	public static void main(String[] args) throws UnknownHostException, SocketException, FileNotFoundException{
		start(4);
	}
	
	public static void start(int lambda) throws UnknownHostException, SocketException, FileNotFoundException{
		Runnable ch = new Channel(lambda);
		Thread cht = new Thread(ch);
		cht.start();
		
		Runnable c1 = new Client(30000, lambda);
		Thread t1 = new Thread(c1);
		Runnable c2 = new Client(40000, lambda);
		Thread t2 = new Thread(c2);
		
		t1.start();
		t2.start();
	}
}
