import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPSender {
	private static final int SENDER_PORT_NUMBER = 30000;
	private static final int RECEIVER_PORT_NUMBER = 50000;
	private static final int BYTE_SIZE = 64000;
	private static final int LOOP_COUNT = 100;

	public static void main(String[] args) throws Exception {
		InetAddress inet = InetAddress.getLocalHost();
		int port = SENDER_PORT_NUMBER;
		byte[] buf = new byte[BYTE_SIZE];
		DatagramPacket packet = new DatagramPacket(buf, buf.length, inet, RECEIVER_PORT_NUMBER);
		DatagramSocket senderSocket = new DatagramSocket(port);
		senderSocket.setSoTimeout(4000);

		//var for timer
		long startTime, endTime, total = 0;
		int count = 0;
		for(int i = 0; i < LOOP_COUNT; i++){
			try{
				startTime = System.currentTimeMillis();
				senderSocket.send(packet);
				senderSocket.receive(packet);
				endTime = System.currentTimeMillis();
				total += (endTime - startTime);
				count++;
			}catch(Exception e){
				System.out.println("Packet lost at " + i);
				
			}
		}
		System.out.println("Sent " + count + " of size " + buf.length + ", took " + total + "ms.");
		senderSocket.close();
	}
}
