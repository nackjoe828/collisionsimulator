import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;


public class Channel implements Runnable{
	private static final int RECEIVER_PORT_NUMBER = 60000;
	private static final int BYTE_SIZE = 1000;
	private static final int TOTAL_SLOT = 50000;
	private static final int SLOT_TIME = 8;
	
	public Channel(int lambda) throws UnknownHostException, SocketException, FileNotFoundException{
		this.lambda = lambda * 10;
		InetAddress inet = InetAddress.getLocalHost();
		byte[] buf = new byte[BYTE_SIZE];
		packet = new DatagramPacket(buf, buf.length, inet, 0);
		socket = new DatagramSocket(RECEIVER_PORT_NUMBER);
		previous = System.currentTimeMillis();
		output = new PrintWriter("channel-" + lambda + ".txt");
	}

	@Override
	public void run() {
		System.out.println("Writing to a file...");
		long start = System.currentTimeMillis();
		while(true){
			try {
				socket.receive(packet);
				current = System.currentTimeMillis();
				long diff = current - start;
				output.print("" + diff + "\t");
				output.println("Received from " + packet.getAddress().toString() + ":" + packet.getPort());
				//check for collision
				if(current - previous < SLOT_TIME){
					byte[] buf = new byte[BYTE_SIZE];
					buf[0] = 1;
					packet.setData(buf);
					output.println("collision");
				}
				output.print("" + diff + "\t");
				output.println("Sending to " + packet.getAddress().toString() + ":" + packet.getPort());
				output.flush();
				socket.send(packet);
				previous = current;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if((System.currentTimeMillis() - start) > TOTAL_SLOT) break;
		}
		socket.close();
	}
	
	private int lambda;
	private long current;
	private long previous;
	private DatagramSocket socket;
	private DatagramPacket packet;
	private PrintWriter output;
}
