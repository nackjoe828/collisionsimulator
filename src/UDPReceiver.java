import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPReceiver {
	private static final int SENDER_PORT_NUMBER = 40000;
	private static final int RECEIVER_PORT_NUMBER = 50000;

	public static void main(String[] args) throws Exception {
		InetAddress inet = InetAddress.getLocalHost();
		int port = RECEIVER_PORT_NUMBER;
		byte[] buf = new byte[4000];
		DatagramPacket packet = new DatagramPacket(buf, buf.length, inet, SENDER_PORT_NUMBER);
		DatagramSocket receiverSocket = new DatagramSocket(port);
		while(true){
			receiverSocket.receive(packet);
			System.out.println("Received from " + packet.getAddress().toString() + ":" + packet.getPort());
			receiverSocket.send(new DatagramPacket(buf, buf.length, packet.getSocketAddress()));
		}
	}
}