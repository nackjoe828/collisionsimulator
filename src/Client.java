import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;


public class Client implements Runnable{
	private static final int RECEIVER_PORT_NUMBER = 60000;
	private static final int BYTE_SIZE = 1000;
	private static final int TOTAL_SLOT = 50000;
	private static final int SLOT_TIME = 8;
	
	public Client(int port, int lambda) throws UnknownHostException, SocketException, FileNotFoundException{
		this.port = port;
		this.lambda = lambda * 10;
		InetAddress inet = InetAddress.getLocalHost();
		byte[] buf = new byte[BYTE_SIZE];
		packet = new DatagramPacket(buf, buf.length, inet, RECEIVER_PORT_NUMBER);
		socket = new DatagramSocket(port);
		rand = new Random();
		output = new PrintWriter("client-" + port + "-" + lambda + ".txt");
	}
	
	public void run(){
		System.out.println("Writing to a file...");
		long start = System.currentTimeMillis();
		while(true){
			try {
				Thread.sleep(rand.nextInt(lambda));
				send();
				output.println("" + (System.currentTimeMillis() - start) + ":sent packet");
				receive();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//wait for next slot
			while(System.currentTimeMillis() % lambda != 0){}
			
			if((System.currentTimeMillis() - start) > TOTAL_SLOT) break;
		}
		socket.close();
	}
	
	/**
	 * send packet after waiting according to backoff algorithm
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public void send() throws IOException, InterruptedException{
		//backoff
		int backoffTime;
		if(collisionFlag){
			int n = (collisionCount >= 10) ? 10 : collisionCount;
			int randInt = (n == 0) ? 0 : rand.nextInt((int)Math.pow(2, n) - 1); // [0, 2^n)
			backoffTime = randInt * SLOT_TIME;
		}
		else
			backoffTime = 0;
		//int randInt = rand.nextInt(1000);
		Thread.sleep(backoffTime);
		output.println("slept for " + backoffTime + "ms");
		byte[] buf = new byte[BYTE_SIZE];
		packet.setData(buf);
		socket.send(packet);
	}
	
	/**
	 * receive packet and check for collision
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public void receive() throws IOException, InterruptedException{
		socket.receive(packet);
		Thread.sleep(SLOT_TIME); //mimic slot time
		byte[] rec = packet.getData();
		//if collision
		if(rec[0] == 1){
			collisionCount++;
			collisionFlag = true;
			output.println("collision detected");
			output.flush();
		}
		else{
			collisionFlag = false;
			output.println("no collision detected");
			output.flush();
		}
	}
	
	private Random rand;
	private int collisionCount;
	private boolean collisionFlag;
	private int lambda;
	private int port;
	private DatagramSocket socket;
	private DatagramPacket packet;
	private PrintWriter output;
}
